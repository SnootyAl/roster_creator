package GUI;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.*;


public class FileHandler extends JPanel implements ActionListener {

    private JPanel menuContainer;
    private JButton open, save, export;
    private JFileChooser filechooser;


    public FileHandler() {

        super();
        open = new JButton("Open");
        open.setActionCommand("open");
        save = new JButton("Save");
        save.setActionCommand("save");
        export = new JButton("Export");
        export.setActionCommand("export");
        add(open);
        add(save);
        add(export);
        filechooser = new JFileChooser();
        open.addActionListener(this);
        save.addActionListener(this);
        export.addActionListener(this);
    }

    public void actionPerformed(ActionEvent event) {

        if (event.getActionCommand() == "open") {

            int returnVal = filechooser.showOpenDialog(FileHandler.this);
            System.out.println("File Opened");
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File VECFile = filechooser.getSelectedFile();
            }
        };

        if (event.getActionCommand() == "save") {
            System.out.println("You should save something");

        };

        if (event.getActionCommand() == "export") {
            System.out.println("Probably do something about exporting right about now");
        };
    }
}