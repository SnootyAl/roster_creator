import java.awt.*;
import java.awt.event.ComponentListener;

import javax.swing.*;
import javax.swing.border.Border;

import GUI.*;

public class GUI extends JFrame {

    public GUI() {
        super("Insert Snappy name");
        setMinimumSize(new Dimension(1000, 400));
        // this.addComponentListener(new ComponentAdapter()  {
        //     public void componentResized(ComponentEvent e) {
        //         Dimension d = this.getSize();
        //         Dimension minD = this.getMinimumSize();
        //         if (d.width < minD.width)
        //             d.width = minD.width;
        //         if (d.height < minD.height)
        //             d.height = minD.height;
        //         this.setSize(d);
        //     }
        // });
    }

    public void CreateAndShowGUI(){
        
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1000, 600);

        JPanel toplevel = new JPanel(new BorderLayout());
        previewWindow preview = new previewWindow();
        toplevel.add(preview, BorderLayout.CENTER);

        FileHandler menubar = new FileHandler();
        toplevel.add(menubar, BorderLayout.NORTH);

        uiPanel userInput = new uiPanel();
        toplevel.add(userInput, BorderLayout.WEST);
        
        
        
        this.getContentPane().add(BorderLayout.CENTER, toplevel);   
        setVisible(true);
        
    }
}